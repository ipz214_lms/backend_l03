<?php 
$monthList = [
    '1' => 'січень',
    'лютий',
    'березень',
    'квітень',
    'травень',
    'червень',
    'липень',
    'серпень',
    'вересень',
    'жовтень',
    'листопад',
    'грудень',
];

function createSelect($name, $values, $selected = null)
{
    $resultString = "<select name=\"{$name}\">";
    foreach ($values as $value => $title) {
        if ($selected == $title or $selected == $value)
            $resultString .= "<option selected = \"selected\" value={$value}>{$title}</option>";
        else
            $resultString .= "<option value={$value}>{$title}</option>";
    }
    return $resultString .= '</select>';
}
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}