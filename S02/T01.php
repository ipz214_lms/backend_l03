<?php 
session_start();
require_once('library.inc.php');
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$date = array(
    'month' => date('m'),
    'day' => date('d'),
    'year' => date('Y'),
);

foreach($date as $k => $v) {
    if(isset($_SESSION[$k])) {
        $date[$k] = $_SESSION[$k];
    }
    if(isset($_POST[$k])) {
         $date[$k] =  $_SESSION[$k] = $_POST[$k];
    }
}


if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $d = "{$date['month']}-{$date['day']}-{$date['year']}";
    if (validateDate($d, 'n-j-Y')) {
        echo $d;
    }  
    else {
        echo 'Помилка';
    }
}
?>

<form action="<?= $_SERVER['PHP_SELF'] ?>" METHOD="post">
    <?= createSelect('day', array_combine($r = range(1, 31), $r), $date['day']) ?>
    <?= createSelect('month', $monthList, $date['month']) ?>
    <?= createSelect('year', array_combine($r = range(1990, date('Y')), $r), $date['year']) ?>
    <button type="submit">Submit</button>
</form>
</body>
</html>
